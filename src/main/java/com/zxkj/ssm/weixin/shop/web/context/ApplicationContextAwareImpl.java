package com.zxkj.ssm.weixin.shop.web.context;
import com.zxkj.ssm.weixin.shop.utils.ResourceUtil;
import com.zxkj.ssm.weixin.shop.utils.SpringContextUtils;
import org.springframework.beans.BeansException;
import org.springframework.context.ApplicationContext;
import org.springframework.context.ApplicationContextAware;
import org.springframework.context.EnvironmentAware;
import org.springframework.core.env.Environment;
import org.springframework.stereotype.Component;

/**
 *@Author 程序媛
 *@Date 2018/7/11 9:04
 *@Description 获取容器实例
 *@Version
 */
@Component
public class ApplicationContextAwareImpl  implements ApplicationContextAware, EnvironmentAware {

    @Override
    public void setApplicationContext(ApplicationContext applicationContext)
            throws BeansException {
        SpringContextUtils.applicationContext = applicationContext;
    }

    @Override
    public void setEnvironment(Environment environment) {
        ResourceUtil.env=environment;
    }
}
